#pragma once

#include <string>
#include <set>

class Tester
{
public:
	/** Single entry point to run the tests desired. */
	void Run() const;

private:
	/** This should point to a file with config for BTTimeProbaTest() */
	const std::string TIME_BT_TEST_FILE = "bt_proba_test.txt";

	/* Tests */
	/** Shows probability we should scout. Configurable from txt file. */
	void BTTimeProbaTest() const;

	/** Computes and plots a probability distribution of all openings for given build tree over a given timespan. */
	void OPDistribTest(const size_t fromTime, const size_t toTime, const std::set<int>& buildTree) const;

	/** Plots a probability that a strategy has changed */
	void ShouldScoutTest(const size_t fromTime, const size_t toTime, const std::set<int>& buildTree) const;

	/** Plots all build trees for given race into separate files */
	void AllBtsDistribPlot(const size_t fromTime, const size_t toTime) const;

	/** Helper function to parse a build tree from string */
	std::set<int> ParseBt(const std::string& buildTree) const;
};