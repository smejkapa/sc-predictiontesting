import os

d1 = "c:\\_SC_Log\\BTS_18_w_OP"
d2 = "c:\\_SC_Log\\BTS_18_wo_OP"
out_dir = "c:\\_SC_Log\\BTS_18_vs"

for _, _, files in os.walk(d1):
    for file in files:
        if ".csv" not in file:
            continue
        file = file[:-4]
        f1 = os.path.join(d1, file)
        f2 = os.path.join(d2, file)
        out_f = os.path.join(out_dir, file)
        print("Plotting: " + file)
        os.system("RScript plotCombine.r " + " ".join([f1, f2, out_f]))
