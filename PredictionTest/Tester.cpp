#include "Tester.h"
#include "PredictionManager.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <exception>
#include <map>
#include <BOOST/filesystem.hpp>
//#include "enums_name_tables_tt.h"

using namespace std;

void Tester::Run() const
{
	std::cout << "Start" << std::endl;
	
	/***** Plot all build trees over time tests *****/
	//AllBtsDistribPlot(1, 18*60);

	
	/***** Should scout tests *****/
	// Do not forget to change the race in PredictionManager.cpp
	// Protoss
	const auto BUILD_TREE = { 0, 3, 5, 7, 10, 11, 12 }; // DT build
	//const auto BUILD_TREE = { 0, 3, 7 }; // DT build
	//const auto BUILD_TREE = { 0, 3, 5, 7, 11 }; // generic tech build

	// Zerg
	//const auto BUILD_TREE = { 0, 19, 1, 18, 14, 4, 13 }; // mutas
	//const auto BUILD_TREE = { 0, 19, 1, 18, 14, 7 }; // hydras
	ShouldScoutTest(1, 10*60, BUILD_TREE);


	/***** Distribution of openings over time tests *****/
	//OPDistribTest(1, 10 * 60, BUILD_TREE);

	/***** Should scout tests configurable from file *****/
	//BTTimeProbaTest();
	
	std::cout << "Tests finished" << std::endl;
}

void Tester::ShouldScoutTest(const size_t fromTime, const size_t toTime, const std::set<int>& buildTree) const
{
	ofstream s;
	s.open(PredictionManager::LOG_DIR + "graph.csv");
	PredictionManager::Instance().SetBuildingTypesSeen(buildTree);
		
	s << "Time; Probability" << endl;
	for (size_t i = fromTime; i < toTime; i++)
	{
		auto change = PredictionManager::Instance().ComputeDistribChangeProba(i);
		//cout << change << endl;
		s << i << "; " << change << endl;
	}
	s.close();

	system("RScript plotData.r");
}

void Tester::AllBtsDistribPlot(const size_t fromTime, const size_t toTime) const
{
	// Compute distributions
	vector<vector<BuildTreeProbaPair> > time_BT;
	// What bts we want distributions for?
	vector<size_t> allBts;
	for (size_t i = 0; i < PredictionManager::Instance().serializedTables.vector_X.size(); i++)
	{
		allBts.push_back(i);
	}
	
	// Compute distributions for all times we want
	for (size_t i = fromTime; i < toTime; i++)
	{
		auto probas = PredictionManager::Instance().GetP_BT_T(i, allBts);
		time_BT.push_back(probas);
	}
		
	// Output distributions
	ofstream s;
	for (size_t buildTreeIdx = 0; buildTreeIdx < allBts.size(); buildTreeIdx++)
	{
		// Make sure we are talking about the same bt
		auto bt = time_BT[0][buildTreeIdx];
		if (bt.index != buildTreeIdx)
			cout << "indices are not the same" << endl;

		// Simple file name convention - all buildings
		string bt_string;
		for (const auto & b : PredictionManager::Instance().serializedTables.vector_X[bt.index])
		{
			bt_string += to_string(b) + "_";
		}
		string btFile = PredictionManager::LOG_DIR + "BTS_18_wo_OP\\" + bt_string;

		cout << "opening bt file " << btFile << endl;
		// Output current bt disribution
		s.open(btFile + ".csv");
		s << "Time; Probability" << endl;
		for (size_t t = 0; t < time_BT.size(); t++)
		{
			s << t << "; " << time_BT[t][buildTreeIdx].probability << endl;
		}
		s.close();

		// Plot the graph
		string cmd = "RScript plotData.r " + btFile + " " + btFile;
		cout << "Plotting " << btFile << endl;
		system(cmd.c_str());
	}
}

void Tester::OPDistribTest(const size_t fromTime, const size_t toTime, const std::set<int>& buildTree) const
{
	const string DIR = PredictionManager::LOG_DIR + "ops_DT_imp\\";
	boost::filesystem::create_directory(DIR);

	vector<unique_ptr<ofstream> > files;
	for (const auto& op : PredictionManager::Instance().serializedTables.openings)
	{
		auto s = make_unique<ofstream>(ofstream());
		s->open(DIR + op + ".csv");
		*s << "Time; Probability" << endl;
		files.push_back(move(s));
	}

	for (size_t i = fromTime; i < toTime; i++)
	{
		auto probas = PredictionManager::Instance().computeDistribOpenings(i, buildTree, true);
		for (size_t j = 0; j < probas.size(); ++j)
		{
			*files[j] << i << ";" << probas[j] << endl;
		}
	}
	size_t i = 0;
	for (const auto& f : files)
	{
		auto op = PredictionManager::Instance().serializedTables.openings[i];
		f->close();
		cout << "Plotting " << op << endl;
		string cmd = "RScript plotData.r " + DIR + op + " " + DIR + op;
		++i;
		system(cmd.c_str());
	}
}

void Tester::BTTimeProbaTest() const
{
	ifstream input(TIME_BT_TEST_FILE);

	int fromTime;
	int toTime;
	set<int> buildTree;

	string line;
	while (getline(input, line))
	{
		auto timePos = line.find(';');
		if (timePos == string::npos)
			throw exception();

		// Time build tree in format "[fromTime], [toTime]; [space divided buildings]" e.g. 123 124; 4 5 6 0
		istringstream(line.substr(0, timePos)) >> fromTime >> toTime;
		buildTree = ParseBt(line.substr(timePos + 1, line.length()));

		PredictionManager::Instance().SetBuildingTypesSeen(buildTree);

		for (int currentTime = fromTime; currentTime <= toTime; ++currentTime)
		{
			auto proba = PredictionManager::Instance().ComputeDistribChangeProba(currentTime);
			cout << "Current change proba is: " << proba << endl;

			char c;
			cin >> c;
		}
	}
}

set<int> Tester::ParseBt(const string& buildTreeToParse) const
{
	set<int> buildTree;
	istringstream iss(buildTreeToParse);

	while (iss.good())
	{
		int building;
		iss >> building;
		buildTree.insert(building);
	}

	return buildTree;
}