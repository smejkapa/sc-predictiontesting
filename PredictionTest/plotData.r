library(ggplot2)

args <- commandArgs(trailingOnly = TRUE)
inputFile <- "c:\\_SC_Log\\graph"
outputFile <- "c:\\_SC_Log\\plot"
title = "PvP - Dark templar opening"
if (length(args) >= 1)
{
	outputFile <- args[1]
}
outputFile <- paste(outputFile, ".png", sep="")

if (length(args) >= 2)
{
	inputFile <- args[2]
}
inputFile <- paste(inputFile, ".csv", sep="")

data <- read.csv(file=inputFile, sep=";", head=TRUE)
ggplot(data=data, aes(x=Time, y=Probability, group=1)) + geom_line(color="red") + expand_limits(x = 0, y = c(0,1)) +
scale_y_continuous(breaks=seq(0,1,0.1)) + scale_x_continuous(breaks=seq(0, max(data$Time), 30)) + ggtitle(title)

ggsave(outputFile, width=14, height=7)
