library(ggplot2)
library(reshape)

args <- commandArgs(trailingOnly = TRUE)

f1 = paste(args[1], ".csv", sep="")
f2 = paste(args[2], ".csv", sep="")
outputFile = paste(args[3], ".png", sep="")


d1 <- read.csv(file=f1, sep=";", head=TRUE)
d2 <- read.csv(file=f2, sep=";", head=TRUE)
df <- data.frame(d1$Time, d1$Probability, d2$Probability)
df.melted <- melt(df, id="d1.Time")

ggplot(data=df.melted, aes(x=d1.Time, y=value, color=variable)) + geom_line() + expand_limits(x = 0, y = c(0,1)) + scale_colour_manual("", values=c("red", "blue"), labels=c("WithOP", "Without"))
ggsave(outputFile, width=14, height=7)
