#pragma once

#include <string>
#include "boost/archive/text_iarchive.hpp"
#include "serialized_tables.h"
#include "CSingleton.h"
#include <set>
#include <list>
#include <queue>

/***
***********************
*openingsProba content*
***********************
|=============|
|Ben's labels:| (extracted with rules)
|=============|

Terran openings, in order (in the vector):
- Bio
- TwoFactory
- VultureHarass
- SiegeExpand
- Standard
- FastDropship
- Unknown

Protoss openings, in order (in the vector):
- FastLegs
- FastDT
- FastObs
- ReaverDrop
- Carrier
- FastExpand
- Unknown

Zerg openings, in order (in the vector):
- TwoHatchMuta
- ThreeHatchMuta
- HydraRush
- Standard
- HydraMass
- Lurker
- Unknown

|=================|
|Gabriel's labels:| (extracted by clustering)
|=================|

Terran openings, in order (in the vector):
- bio
- rax_fe
- two_facto
- vultures
- drop
- unknown

Protoss openings, in order (in the vector):
- two_gates
- fast_dt
- templar
- speedzeal
- corsair
- nony
- reaver_drop
- unknown

Zerg openings, in order (in the vector):
- fast_mutas
- mutas
- lurkers
- hydras
- unknown
*/

//#define _PREDICTION_MANAGER_DEBUG // Unused macro only viable when in game

/** Minimal probability to avoid using 0 */
#define MIN_PROB 0.00000000000000000001;
/** Exmerimental define to use a single prior throughout the experient */
//#define SINGLE_PRIOR
/** Experimental define to use prior with fixed time frame behind */
//#define USE_MY_PRIOR

struct OpeningProbaPair
{
	OpeningProbaPair(const std::string& name, long double proba)
		: opening(name), probability(proba) {}
	
	std::string opening;
	long double probability;

	bool operator< (const OpeningProbaPair& str) const
	{
		return (probability < str.probability);
	}
	static bool descending(OpeningProbaPair a, OpeningProbaPair b)
	{
		return b < a;
	}
};

struct BuildTreeProbaPair
{
	BuildTreeProbaPair(const size_t index, long double proba)
		: index(index), probability(proba) {}
	
	/** Index into the serialized table of learned build trees */
	size_t index;

	/** Probability that this build tree is the one our opponent has at current time */
	long double probability;
};

class PredictionManager : public CSingleton<PredictionManager>
{
public:
	enum Race
	{
		Protoss,
		Terran,
		Zerg
	};
	std::string RaceToString(Race race) const;
	const static std::string TABLES_DIR;
	const static std::string LOG_DIR;

private:
	friend class CSingleton < PredictionManager > ;

	PredictionManager();
public:
	const Race enemyRace;

	static const int LEARNED_TIME_LIMIT = 1080; // 18 minutes
	static const int __ETECHESTIMATOR_MINUTES__ = 2; // 2 minutes further/later

	serialized_tables serializedTables;
	openings_knowing_player op_prior;

	std::vector<long double> openingsProbas; // see the big fat comment above

	bool hasInfered;
	bool tableLoaded;

	void LoadTable(const std::string& tablePath);

	void InitializePrior(const Race race);
	int format_max_time(int t);

	std::set<int> buildingsTypesSeen;

	std::vector<long double> computeVecDistribOpenings(int time, const std::set<int>& observations);
	//void computeDistribOpenings(int time);
	std::vector<long double> computeDistribOpenings(int time, const std::set<int>& observations, bool computePrior);
	std::vector<long double> computeDistribOpenings(int time, const std::set<int>& observations);
	inline bool testBuildTreePossible(int indBuildTree, const std::set<int>& setObs);

	// Checking serialized tables
	std::set<int> learnedBuildings;
	void getLearnedBuildings();
	void logAllBuildTrees();

	int lastPriorTime;
	
	std::vector<std::list<long double> > myPrior;
	std::vector<long double> currentPrior;

	// Vector of missing units from Gabriel's serialized tables
	std::set<int> missingUnitTypes;
public:
	std::vector<BuildTreeProbaPair> GetP_BT_T(int time, const std::vector<size_t>& buildTrees);
	//const std::vector<long double>& getOpeningsProbas() const;
	//std::vector<long double> getOpeningsProbasIn(int time);
	//const std::vector<OpeningProbaPair> GetOpeningProbas(int time);
	const std::vector<OpeningProbaPair> GetOpeningProbas(const std::set<int>& observations, int time);

	double ComputeDistribChangeProba(const int time);

	void SetBuildingTypesSeen(const std::set<int>& buildings) { buildingsTypesSeen = buildings; }
	std::set<int> GetBuildingTypesSeen() const { return buildingsTypesSeen; }
};