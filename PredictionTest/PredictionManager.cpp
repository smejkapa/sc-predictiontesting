#include "PredictionManager.h"
#include <fstream>
#include "enums_name_tables_tt.h"
#include <iostream>

const std::string PredictionManager::TABLES_DIR = "C:\\__SC\\tables\\";
const std::string PredictionManager::LOG_DIR = "C:\\_SC_Log\\";

PredictionManager::PredictionManager()
	: tableLoaded(false), hasInfered(false), enemyRace(Race::Protoss), lastPriorTime(0)
{
	// Load the learned prob tables (uniforms + bell shapes) for the right match up
	// This is not obfuscation, the learning of these tables is described in the CIG 2011 Paper
	// A Bayesian Model for Opening Prediction in RTS Games with Application to StarCraft, Gabriel Synnaeve, Pierre Bessiere, CIG (IEEE) 2011
	// All code for the learning is here: https://github.com/SnippyHolloW/OpeningTech
	{
		std::string serializedTablesFileName(TABLES_DIR);

		switch (enemyRace)
		{
		case PredictionManager::Protoss:
			serializedTablesFileName.append("PvP.table");
			LoadTable(serializedTablesFileName);
			break;
		case PredictionManager::Terran:
			serializedTablesFileName.append("TvP.table");
			LoadTable(serializedTablesFileName);
			break;
		case PredictionManager::Zerg:
			serializedTablesFileName.append("ZvP.table");
			LoadTable(serializedTablesFileName);
			break;
		default:
			break;
		}
	}

	// Initialize openingsProbas with a uniform distribution
	size_t nbOpenings = serializedTables.openings.size();
	openingsProbas = std::vector<long double>(nbOpenings, 1.0 / nbOpenings);

	// Initialize prior as uniform for now
	InitializePrior(enemyRace);
}

void PredictionManager::getLearnedBuildings()
{
	std::ofstream s;
	std::ofstream known_s;
	std::string output = LOG_DIR + "units_" + RaceToString(enemyRace) + ".txt";
	std::string knownOutput = LOG_DIR + "units_known_" + RaceToString(enemyRace) + ".txt";
	s.open(output.c_str(), std::ofstream::out);
	known_s.open(knownOutput.c_str(), std::ofstream::out);
	
	for (size_t i = 0; i < serializedTables.vector_X.size(); i++)
	{
		for (const auto& j : serializedTables.vector_X[i])
		{
			learnedBuildings.insert(j);
		}
	}

	missingUnitTypes.clear();
	for (size_t i = 0; i < 50; ++i)
	{
		if (learnedBuildings.find(i) != learnedBuildings.end())
		{
			s << i << std::endl;
		}
		else
		{
			s << i << " missing" << std::endl;
			missingUnitTypes.insert(i);
		}
	}
	s.close();

	for (const auto& it : learnedBuildings)
	{
		known_s << it << std::endl;
	}
	known_s.close();
}
void PredictionManager::logAllBuildTrees()
{
	std::ofstream s;
	std::string output = LOG_DIR + "bts_" + RaceToString(enemyRace) + ".txt";
	s.open(output.c_str(), std::ofstream::out);

	for (size_t i = 0; i < serializedTables.vector_X.size(); i++)
	{
		for (std::set<int>::const_iterator j = serializedTables.vector_X[i].begin(); j != serializedTables.vector_X[i].end(); ++j)
		{
			s << *j << " ";
		}
		s << std::endl;
	}
	s.close();
}

void PredictionManager::LoadTable(const std::string& tablePath)
{
	std::ifstream input(tablePath.c_str());
	boost::archive::text_iarchive iArchive(input);

	iArchive >> serializedTables;
	tableLoaded = true;

	//getLearnedBuildings();
	//logAllBuildTrees();
}
void PredictionManager::InitializePrior(Race race)
{
	// Setup for given race - Protoss is default
	serialized_tables* serializedTables_ref = &serializedTables;

	// If we don't know the race we want to populate all containers
	// If we know the race, we already only populated the right containers (serializedTables)
	// and others - TvP are empty
	
	// we never played against this opponent => uniform prior
	size_t nbOpenings = serializedTables_ref->openings.size();
#ifdef USE_MY_PRIOR
	for (size_t i = 0; i < nbOpenings; i++)
	{
		myPrior.push_back(std::list<long double>());
		myPrior[i].push_front(1.0 / nbOpenings);
		currentPrior.push_back(1.0 / nbOpenings);
	}
#else
	op_prior.numberGames = std::vector<int>(LEARNED_TIME_LIMIT / 60, 1);
	for (int t = 0; t < LEARNED_TIME_LIMIT / 60; ++t)
		op_prior.tabulated_P_Op_knowing_Time.push_back(std::vector<long double>(nbOpenings, 1.0 / nbOpenings));
#endif
}

std::vector<long double> PredictionManager::computeDistribOpenings(int time, const std::set<int>& observations, bool computePrior)
{
	int t = format_max_time(time);

#ifdef SINGLE_PRIOR
	if (computePrior)
	{
		// Previous minute prior must influence current minute prior
		if (lastPriorTime < t)
		{
			op_prior.tabulated_P_Op_knowing_Time[t] = op_prior.tabulated_P_Op_knowing_Time[lastPriorTime];
			op_prior.numberGames[t] = op_prior.numberGames[lastPriorTime];
		}
		lastPriorTime = t;
	}
#endif

	openingsProbas = computeVecDistribOpenings(time, observations);

	if (computePrior)
	{
		long double priorSum = 0.0;
		long double myPSum = 0.0;
		for (size_t i = 0; i < openingsProbas.size(); ++i)
		{
#ifdef USE_MY_PRIOR
			myPrior[i].push_front(openingsProbas[i]);
			if (myPrior[i].size() > 10)
			{
				myPrior[i].pop_back();
			}
			
			long double sum = 0.0;
			for (const auto& it : myPrior[i])
			{
				sum += it;
			}

			currentPrior[i] = sum / myPrior[i].size();

			myPSum += currentPrior[i];
			

			/*std::cout << "sum: " << sum << std::endl;
			std::cout << currentPrior[i] << std::endl;*/
			
#else
			// cumulative moving average: ca_{n+1} = (x_{n+1}+n*ca_{n})/(n+1)
			op_prior.tabulated_P_Op_knowing_Time[t][i] =
				(openingsProbas[i] + op_prior.numberGames[t] * op_prior.tabulated_P_Op_knowing_Time[t][i])
				/ (op_prior.numberGames[t] + 1);
			
			priorSum += op_prior.tabulated_P_Op_knowing_Time[t][i];
			++op_prior.numberGames[t];
#endif
			/*std::cout << "--\nmy: " << currentPrior[i] << std::endl;
			std::cout << "gs: " << op_prior.tabulated_P_Op_knowing_Time[t][i] << std::endl;*/
		}
		/*std::cout << "mypriSum" << myPSum << std::endl;
		std::cout << "priorSum" << priorSum << std::endl;*/
	}

	return openingsProbas;
}

std::vector<long double> PredictionManager::computeDistribOpenings(int time, const std::set<int>& observations)
{
	// Recompute prior only if we are inferring based on current game state - not speculating
	return  computeDistribOpenings(time, observations, buildingsTypesSeen == observations);
}

// Computes probabilities of openings
std::vector<long double> PredictionManager::computeVecDistribOpenings(int time, const std::set<int>& observations)
{
	// Initialize as a uniform distribution
	std::vector<long double> ret(openingsProbas.size(), 1.0 / openingsProbas.size());
	/// time in seconds
	if (time >= LEARNED_TIME_LIMIT || time <= 0)
	{
		//cout << time << ": time limit reached, returning uniform distribution\n";
		return ret; // we don't know and thus give back the uniform distribution
	}

	size_t nbXes = serializedTables.vector_X.size();
	std::vector<unsigned int> compatibleXes;
	
	// Select Build trees compatible with observations from all learned build trees
	for (size_t i = 0; i < nbXes; ++i)
	{
		if (testBuildTreePossible(i, observations))
			compatibleXes.push_back(i);
	}

	long double runningSum = 0.0;
	// Each opening has a probability summed of probabilities of all build trees at a given time
	for (size_t i = 0; i < openingsProbas.size(); ++i)
	{
		long double sumX = MIN_PROB;
		for (std::vector<unsigned int>::const_iterator it = compatibleXes.begin(); it != compatibleXes.end(); ++it)
		{ /// perhaps underflow? log-prob?
			sumX += 
#ifdef USE_MY_PRIOR
				currentPrior[i]
#else
				op_prior.tabulated_P_Op_knowing_Time[format_max_time(time)][i]
#endif
				* serializedTables.tabulated_P_X_Op[(*it) * openingsProbas.size() + i]
				* serializedTables.tabulated_P_Time_X_Op[(*it) * openingsProbas.size() * LEARNED_TIME_LIMIT + i * LEARNED_TIME_LIMIT + time];
		}
		ret[i] *= sumX;
		runningSum += ret[i];
	}
	long double verifSum = 0.0;

	// Normalize probabilities
	for (size_t i = 0; i < openingsProbas.size(); ++i)
	{
		ret[i] /= runningSum;
		if (ret[i] != ret[i] // test for NaN / 1#IND
			//|| openingsProbas[i] < MIN_PROB        // min proba
			)
		{
			ret[i] = MIN_PROB;
		}
		verifSum += ret[i];
	}

	// Verify that sum of probabilities is in given error range
	if (verifSum < 0.99 || verifSum > 1.01)
	{
		for (size_t i = 0; i < openingsProbas.size(); ++i)
			ret[i] /= verifSum;
	}

	/*if (compatibleXes.size() == 0)
	{
		cout << time << ": No compatible build trees found\n";
		cout << "\tObservations:\n";
		for (set<int>::const_iterator it = observations.begin(); it != observations.end(); ++it)
		{
			cout << "\t" << *it << "\n";
		}
	}
	if (ret.size() > 2 && abs(ret[0] - ret[1]) < 0.001 && abs(ret[1] - ret[2]) < 0.001)
	{
		cout << time << ": Returning uniform distribution when shouldn't\n";
		cout << "\tBuild trees: " << compatibleXes.size() << "\n";
		for(const auto & x : compatibleXes)
		{
			cout << "\t BT: " << endl << "\t\t";
			for (const auto & b : serializedTables.vector_X[x])
			{
				cout << b << ", ";
			}
			cout << endl;
		}
	}*/

	return ret;
}

/**
* Tests if the given build tree (X) value
* is compatible with observations (what have been seen)
* {X ^ observed} covers all observed if X is possible
* so X is impossible if {observed \ {X ^ observed}} != {}
* => X is compatible with observations if it covers them fully
*/
bool PredictionManager::testBuildTreePossible(int indBuildTree, const std::set<int>& setObs)
{
	// Go through all observations
	// If we saw something that is not in the BT return false -> this BT is not possible
	for (std::set<int>::const_iterator it = setObs.begin(); it != setObs.end(); ++it)
	{
		// Ignore all units we don't have build tree for
		if (!serializedTables.vector_X[indBuildTree].count(*it) && !missingUnitTypes.count(*it))
			return false;
	}
	return true;
}

/**
* Computes P(BT|T) * P(T) from the Bayesian network
* P(BT|T) = alpha * sum_OP( P(T|BT, OP) * P(BT|OP) * P(OP) )
* Note that P(OP) is P(OP|T)
*/
std::vector<BuildTreeProbaPair> PredictionManager::GetP_BT_T(int time, const std::vector<size_t>& buildTrees)
{
	long double BASE_PROBA = 1.0 / buildTrees.size(); // Simulate init as uniform distribution
	std::vector<BuildTreeProbaPair> probabilities;
	long double runningSum = 0.0;

	for (std::vector<size_t>::const_iterator bt = buildTrees.begin(); bt != buildTrees.end(); ++bt)
	{
		long double proba = MIN_PROB;
		//set<int> buildings = serializedTables.vector_X[*bt];

		// For each Opening
		for (size_t i = 0; i < openingsProbas.size(); ++i)
		{
			proba +=
#ifdef USE_MY_PRIOR
				currentPrior[i] *
#else
				op_prior.tabulated_P_Op_knowing_Time[format_max_time(time)][i] * // P(OP|T) = prior
#endif
				serializedTables.tabulated_P_X_Op[(*bt) * openingsProbas.size() + i] * // P(BT|OP)
				serializedTables.tabulated_P_Time_X_Op[(*bt) * openingsProbas.size() * LEARNED_TIME_LIMIT + i * LEARNED_TIME_LIMIT + time]; // P(T|BT, OP)
		}

		probabilities.push_back(BuildTreeProbaPair(*bt, BASE_PROBA * proba));
		runningSum += BASE_PROBA * proba;
	}
	
	long double verifSum = 0.0;
	// Normalize probabilities
	for (size_t i = 0; i < probabilities.size(); ++i)
	{
		probabilities[i].probability /= runningSum;
		if (probabilities[i].probability != probabilities[i].probability) // test for NaN / 1#IND
		{
			probabilities[i].probability = MIN_PROB;
		}
		verifSum += probabilities[i].probability;
	}

	// Verify that sum of probabilities is in given error range
	if (verifSum < 0.99 || verifSum > 1.01)
	{
		for (size_t i = 0; i < openingsProbas.size(); ++i)
			probabilities[i].probability /= verifSum;
	}

	return probabilities;
}

/**
* Computes the probability our distribution changes (the most probable opening changes)
* based on the probability of our opponent's current build tree (not only the observations, but what he probably actually has)
*/
double PredictionManager::ComputeDistribChangeProba(const int time)
{
	double proba = 0.0;

	// Get all possible build trees compatible with current observations
	size_t nbXes = serializedTables.vector_X.size();
	std::vector<size_t> compatibleXes;
	for (size_t i = 0; i < nbXes; ++i)
	{
		if (testBuildTreePossible(i, buildingsTypesSeen))
			compatibleXes.push_back(i);
	}

	// Get probability that our opponent has given build tree at given time - for each BT
	std::vector<BuildTreeProbaPair> BTProbas = GetP_BT_T(time, compatibleXes);

	// Get the most probable opening right now
	std::vector<OpeningProbaPair> currentDistrib = GetOpeningProbas(buildingsTypesSeen, time);
	sort(currentDistrib.begin(), currentDistrib.end(), OpeningProbaPair::descending);
	OpeningProbaPair currentOpening = currentDistrib.front();

	for (const auto & bt : BTProbas)
	{
		std::vector<OpeningProbaPair> newDistrib = GetOpeningProbas(serializedTables.vector_X[bt.index], time);
		sort(newDistrib.begin(), newDistrib.end(), OpeningProbaPair::descending);
		OpeningProbaPair newOpening = newDistrib.front();
		proba += newOpening.opening != currentOpening.opening ? bt.probability : 0;

		if (bt.probability < 0)
		{
			std::cout << "wtf " << bt.probability << std::endl;
		}
	}

	return proba;
}

const std::vector<OpeningProbaPair> PredictionManager::GetOpeningProbas(const std::set<int>& observations, int time)
{
	std::vector<long double> probas = computeDistribOpenings(time, observations);
	std::vector<OpeningProbaPair> openProba;
	for (size_t i = 0; i < probas.size(); ++i)
		openProba.push_back(OpeningProbaPair(serializedTables.openings[i], probas[i]));

	return openProba;
}

/** Returns minimum of learned time limit or given time in minutes */
int PredictionManager::format_max_time(int t)
{
	return std::min((LEARNED_TIME_LIMIT - 1) / 60, t / 60);
}
std::string PredictionManager::RaceToString(Race race) const
{
	switch (race)
	{
	case PredictionManager::Protoss:
		return "Protoss";
	case PredictionManager::Terran:
		return "Terran";
	case PredictionManager::Zerg:
		return "Zerg";
	default:
		return "Error";
	}
}