### What is this for? ###

* This is a standalone project to test various aspects of PredictionManager from [Countering UAlbertaBot](https://bitbucket.org/smejkapa/counteringbwbot/overview)

### How to setup ###

1. Install Visual Studio 2015, the software is a VS2015 project and was compiled with msvc140.
1. Install BOOST3 libraries 1.50+ for msvc140 32-bit.
1. Create system environment variable BOOST_DIR which points to the directory you installed BOOST into.
1. Install R project4. (R is not necessary but is used to plot the results)
1. Make sure R project is in your system path.
1. Open the project in Visual Studio.
1. Setup TABLES_DIR in PredictionTester.cpp to where you have stored the “tables” directory. (serialized tables)
    * The tables represent the dataset used and are available online from https://www.dropbox.com/s/5h5altt533fs06r/tables.zip?dl=0
1. Setup LOGGER_DIR in PredictionTester.cpp to where you want the programs output and intermediate files.
1. All should be set now. You can build and run the project with various settings we explain below.

### How to use ###

There are two important classes in the project. Tester and PredictionManager. Tester is a class of a single public function Run() in which various other functions may be called. These other functions are designed to “test” different features and concepts of PredictionManager. PredictionManager is implementation of Gabriel Synnaeve’s probabilistic model for opening prediction5. The manager is mostly the same as in our bot project. The main difference is that here the whole manager is made public to test all possible concepts.
To use the Tester simply add or remove functions to the Run() and run the project. We have implemented several functions to test. Much more can however be done such as whole match simulation by dynamically adding observations etc.

### Who do I talk to? ###

If you have any issues feel free to mail me.